package com.tcb.hiveInternalTables;


import java.sql.Timestamp;

public class TaxiTrip {

	Timestamp dropoff_datetime;

	float dropoff_latitude;

	float dropoff_longitude;

	int dropoff_taxizone_id;

	float ehail_fee;

	float extra;

	float fare_amount;

	float improvement_surcharge;

	float mta_tax;

	int passenger_count;

	String payment_type;

	Timestamp pickup_datetime;

	float pickup_latitude;

	float pickup_longitude;

	int pickup_taxizone_id;

	int rate_code_id;

	String store_and_fwd_flag;

	float tip_amount;

	float tolls_amount;

	float total_amount;

	float trip_distance;

	String trip_type;

	String vendor_id;

	long trip_id;

	float salary;

	String destignation;

	public Timestamp getDropoff_datetime() {
		return dropoff_datetime;
	}

	public float getSalary() {
		return salary;
	}

	public String getDestignation() {
		return destignation;
	}

	public void setDropoff_datetime(Timestamp dropoff_datetime) {
		this.dropoff_datetime = dropoff_datetime;
	}

	public float getDropoff_latitude() {
		return dropoff_latitude;
	}

	public void setDropoff_latitude(float dropoff_latitude) {
		this.dropoff_latitude = dropoff_latitude;
	}

	public float getDropoff_longitude() {
		return dropoff_longitude;
	}

	public void setDropoff_longitude(float dropoff_longitude) {
		this.dropoff_longitude = dropoff_longitude;
	}

	public int getDropoff_taxizone_id() {
		return dropoff_taxizone_id;
	}

	public void setDropoff_taxizone_id(int dropoff_taxizone_id) {
		this.dropoff_taxizone_id = dropoff_taxizone_id;
	}

	public float getEhail_fee() {
		return ehail_fee;
	}

	public void setEhail_fee(float ehail_fee) {
		this.ehail_fee = ehail_fee;
	}

	public float getExtra() {
		return extra;
	}

	public void setExtra(float extra) {
		this.extra = extra;
	}

	public float getFare_amount() {
		return fare_amount;
	}

	public void setFare_amount(float fare_amount) {
		this.fare_amount = fare_amount;
	}

	public float getImprovement_surcharge() {
		return improvement_surcharge;
	}

	public void setImprovement_surcharge(float improvement_surcharge) {
		this.improvement_surcharge = improvement_surcharge;
	}

	public float getMta_tax() {
		return mta_tax;
	}

	public void setMta_tax(float mta_tax) {
		this.mta_tax = mta_tax;
	}

	public int getPassenger_count() {
		return passenger_count;
	}

	public void setPassenger_count(int passenger_count) {
		this.passenger_count = passenger_count;
	}

	public String getPayment_type() {
		return payment_type;
	}

	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}

	public Timestamp getPickup_datetime() {
		return pickup_datetime;
	}

	public void setPickup_datetime(Timestamp pickup_datetime) {
		this.pickup_datetime = pickup_datetime;
	}

	public float getPickup_latitude() {
		return pickup_latitude;
	}

	public void setPickup_latitude(float pickup_latitude) {
		this.pickup_latitude = pickup_latitude;
	}

	public float getPickup_longitude() {
		return pickup_longitude;
	}

	public void setPickup_longitude(float pickup_longitude) {
		this.pickup_longitude = pickup_longitude;
	}

	public int getPickup_taxizone_id() {
		return pickup_taxizone_id;
	}

	public void setPickup_taxizone_id(int pickup_taxizone_id) {
		this.pickup_taxizone_id = pickup_taxizone_id;
	}

	public int getRate_code_id() {
		return rate_code_id;
	}

	public void setRate_code_id(int rate_code_id) {
		this.rate_code_id = rate_code_id;
	}

	public String getStore_and_fwd_flag() {
		return store_and_fwd_flag;
	}

	public void setStore_and_fwd_flag(String store_and_fwd_flag) {
		this.store_and_fwd_flag = store_and_fwd_flag;
	}

	public float getTip_amount() {
		return tip_amount;
	}

	public void setTip_amount(float tip_amount) {
		this.tip_amount = tip_amount;
	}

	public float getTolls_amount() {
		return tolls_amount;
	}

	public void setTolls_amount(float tolls_amount) {
		this.tolls_amount = tolls_amount;
	}

	public float getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(float total_amount) {
		this.total_amount = total_amount;
	}

	public float getTrip_distance() {
		return trip_distance;
	}

	public void setTrip_distance(float trip_distance) {
		this.trip_distance = trip_distance;
	}

	public String getTrip_type() {
		return trip_type;
	}

	public void setTrip_type(String trip_type) {
		this.trip_type = trip_type;
	}

	public String getVendor_id() {
		return vendor_id;
	}

	public void setVendor_id(String vendor_id) {
		this.vendor_id = vendor_id;
	}

	public long getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(long trip_id) {
		this.trip_id = trip_id;
	}

}
