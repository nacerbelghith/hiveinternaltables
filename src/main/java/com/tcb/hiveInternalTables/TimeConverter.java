package com.tcb.hiveInternalTables;


import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;

import org.apache.commons.lang.time.DateUtils;
import org.apache.parquet.example.data.simple.NanoTime;
import org.apache.parquet.io.api.Binary;

//Timestamp converter: convert a biary format to dateS
public class TimeConverter {

    static ZoneId zone = ZoneId.of("Africa/Tunis");
    public static final long NANOS_PER_MILLISECOND = 1000000;
    public static final long JULIAN_DAY_NUMBER_FOR_UNIX_EPOCH = 2440588;

    public static long getDateTimeValueFromBinary(Binary binaryTimeStampValue, boolean retainLocalTimezone) {
        // This method represents binaryTimeStampValue
        NanoTime nt = NanoTime.fromBinary(binaryTimeStampValue);
        int julianDay = nt.getJulianDay();
        long nanosOfDay = nt.getTimeOfDayNanos();
        long dateTime = (julianDay - JULIAN_DAY_NUMBER_FOR_UNIX_EPOCH) * DateUtils.MILLIS_PER_DAY
                + nanosOfDay / NANOS_PER_MILLISECOND;
        if (retainLocalTimezone) {

            Instant instant = Instant.ofEpochMilli(dateTime).atZone(ZoneOffset.UTC).withZoneSameLocal(zone)
                    .toInstant();
            return instant.toEpochMilli();
        } else {
            return dateTime;
        }
        
    }
    
    public static Timestamp getTimestamp(long dateTime){
    	Timestamp stamp = new Timestamp(dateTime);
    	return stamp;
    }
}