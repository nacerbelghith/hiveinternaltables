package com.tcb.hiveInternalTables;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.hadoop.fs.Path;
import org.apache.parquet.example.data.Group;
import org.apache.parquet.hadoop.ParquetReader;
import org.apache.parquet.hadoop.example.GroupReadSupport;

/**
 *
 * Writer : nassereddine belghith mail : nasreedine.belguith@gmail.com
 */
public class App {
	private static String driverName = "org.apache.hive.jdbc.HiveDriver";

	public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException {
		// try {
		String stringPath = "hdfs://172.17.0.2:8020/user/hive/";
		Path path = new Path(stringPath);
		String sql = "";

		Class.forName(driverName);
		Connection connection = null;
		System.out.println("Before getting connection");
		connection = DriverManager.getConnection("jdbc:hive2://localhost:10000/", "root", "hadoop");
		// create statement
		Statement stmt = connection.createStatement();

		System.out.println("After getting connection " + connection);

		// Create a database taxi
		stmt.executeUpdate("CREATE DATABASE IF NOT EXISTS NyTaxi");
		// Create a new connection to the taxi DATABASE
		Connection connection1 = DriverManager.getConnection("jdbc:hive2://localhost:10000/NyTaxi", "root", "hadoop");
		// Create a table called TaxiDriver
		Statement stmt1 = connection1.createStatement();

		stmt1.execute("CREATE TABLE IF NOT EXISTS "
				+ " nytaxi (dropoff_datetime Timestamp , dropoff_latitude DECIMAL, dropoff_longitude DECIMAL, dropoff_taxizone_id int, ehail_fee DECIMAL,extra DECIMAL,fare_amount DECIMAL,improvement_surcharge DECIMAL,mta_tax DECIMAL, passenger_count int, payment_type String , pickup_datetime Timestamp, pickup_latitude DECIMAL, pickup_longitude DECIMAL,pickup_taxizone_id int, rate_code_id int, store_and_fwd_flag int,tip_amount DECIMAL, tolls_amount DECIMAL,total_amount DECIMAL, trip_distance DECIMAL, trip_type String,vendor_id String, trip_id INT, "
				+ " salary String, destignation String \n)" + " COMMENT 'Employee details' " + " ROW FORMAT DELIMITED "
				+ " FIELDS TERMINATED BY ','" + " LINES TERMINATED BY '\n'" + " STORED AS TEXTFILE");
		System.out.println("Table nytaxi created.");

		sql = "describe " + "Nytaxi.nytaxi";
		System.out.println("Running: " + sql);
		stmt.executeQuery(sql);

		// load data into table
		// NOTE: filepath has to be in the TCB server but since i work from home i used
		// my HDFS path
		String tableName = "Nytaxi.nytaxi";
		try {
			org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
			ParquetReader<Group> reader = ParquetReader.builder(new GroupReadSupport(), path).withConf(configuration)
					.build();

			Group group = null;
			for (int i = 0; i < 100; i++) {
				group = reader.read();
				TaxiTrip trip = new TaxiTrip();
				try {
					trip.setDropoff_datetime(TimeConverter.getTimestamp(
							TimeConverter.getDateTimeValueFromBinary(group.getInt96("dropoff_datetime", 0), true)));
					System.out.println(trip.dropoff_datetime);
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				try {
					trip.setDropoff_latitude(group.getFloat("dropoff_latitude", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setDropoff_longitude(group.getFloat("dropoff_longitude", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setDropoff_taxizone_id(group.getInteger("dropoff_taxizone_id", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setEhail_fee(group.getFloat("ehail_fee", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setExtra(group.getFloat("extra", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setFare_amount(group.getFloat("fare_amount", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setImprovement_surcharge(group.getFloat("improvement_surcharge", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setMta_tax(group.getFloat("mta_tax", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setPassenger_count(group.getInteger("passenger_count", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setPayment_type(group.getString("payment_type", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setPickup_datetime(TimeConverter.getTimestamp(
							TimeConverter.getDateTimeValueFromBinary(group.getInt96("pickup_datetime", 0), true)));
				} catch (Exception e) {
				}
				;
				try {
					trip.setPickup_latitude(group.getFloat("pickup_latitude", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setPickup_longitude(group.getFloat("pickup_longitude", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setPickup_taxizone_id(group.getInteger("pickup_taxizone_id", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setRate_code_id(group.getInteger("rate_code_id", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setStore_and_fwd_flag(group.getString("store_and_fwd_flag", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setTip_amount(group.getFloat("tip_amount", 0));
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				try {
					trip.setTolls_amount(group.getFloat("tolls_amount", 0));
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				try {
					trip.setTotal_amount(group.getFloat("total_amount", 0));
				} catch (Exception e) {
				}
				;
				try {
					trip.setTrip_distance(group.getFloat("trip_distance", 0));
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				try {
					trip.setTrip_type(group.getString("trip_type", 0));
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				try {
					trip.setVendor_id(group.getString("vendor_id", 0));
				} catch (Exception e) {
					e.printStackTrace();
				}
				;
				try {
					trip.setTrip_id(group.getLong("trip_id", 0));
				} catch (Exception e) {
					e.printStackTrace();
				}
				;

				////////////////////////////////////////////////////////////////////////////////////////

				sql = "INSERT INTO TABLE " + tableName + " VALUES('" + String.valueOf(trip.getDropoff_datetime())
						+ "','" + String.valueOf(trip.dropoff_latitude) + "','" + String.valueOf(trip.dropoff_longitude)
						+ "','" + String.valueOf(trip.dropoff_taxizone_id) + "','" + String.valueOf(trip.ehail_fee)
						+ "','" + String.valueOf(trip.extra) + "','" + String.valueOf(trip.fare_amount) + "','"
						+ String.valueOf(trip.improvement_surcharge) + "','" + String.valueOf(trip.mta_tax) + "','"
						+ String.valueOf(trip.passenger_count) + "','" + String.valueOf(trip.payment_type) + "','"
						+ String.valueOf(trip.pickup_datetime) + "','" + String.valueOf(trip.pickup_latitude) + "','"
						+ String.valueOf(trip.pickup_longitude) + "','" + String.valueOf(trip.pickup_taxizone_id)
						+ "','" + String.valueOf(trip.pickup_taxizone_id) + "','"
						+ String.valueOf(trip.pickup_taxizone_id) + "','" + String.valueOf(trip.tip_amount) + "','"
						+ String.valueOf(trip.tolls_amount) + "','" + String.valueOf(trip.total_amount) + "','"
						+ String.valueOf(trip.trip_distance) + "','" + String.valueOf(trip.trip_type) + "','"
						+ String.valueOf(trip.vendor_id) + "','" + String.valueOf(trip.trip_id) + "','"
						+ String.valueOf(trip.salary) + "','" + String.valueOf(trip.destignation) + "\n')";
				System.out.println("Running: " + sql);
				ResultSet resultSet = stmt.executeQuery(sql);
				
			}
			sql = "DELETE FROM"+ tableName + "[ WHERE col1 is null]";
			System.out.println("Running: " + sql);
			stmt.executeQuery(sql);

		} catch (Exception e) {
			e.printStackTrace();
		}
		;
	}
}